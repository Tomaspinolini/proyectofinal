document.getElementById("btn__iniciar-sesion").addEventListener("click", iniciarSesion);
document.getElementById("btn__registrarse").addEventListener("click", register);
window.addEventListener("resize", anchoPage);

var formulario_login = document.querySelector(".formulario__login");
var formulario_register = document.querySelector(".formulario__register");
var contenedor_login_register = document.querySelector(".contenedor__login-register");
var caja_trasera_login = document.querySelector(".caja__trasera-login");
var caja_trasera_register = document.querySelector(".caja__trasera-register");

function anchoPage(){

    if (window.innerWidth > 850){
        caja_trasera_register.style.display = "block";
        caja_trasera_login.style.display = "block";
    }else{
        caja_trasera_register.style.display = "block";
        caja_trasera_register.style.opacity = "1";
        caja_trasera_login.style.display = "none";
        formulario_login.style.display = "block";
        contenedor_login_register.style.left = "0px";
        formulario_register.style.display = "none";   
    }
}

anchoPage();


    function iniciarSesion(){
        if (window.innerWidth > 850){
            formulario_login.style.display = "block";
            contenedor_login_register.style.left = "10px";
            formulario_register.style.display = "none";
            caja_trasera_register.style.opacity = "1";
            caja_trasera_login.style.opacity = "0";
        }else{
            formulario_login.style.display = "block";
            contenedor_login_register.style.left = "0px";
            formulario_register.style.display = "none";
            caja_trasera_register.style.display = "block";
            caja_trasera_login.style.display = "none";
        }
    }

    function register(){
        if (window.innerWidth > 850){
            formulario_register.style.display = "block";
            contenedor_login_register.style.left = "410px";
            formulario_login.style.display = "none";
            caja_trasera_register.style.opacity = "0";
            caja_trasera_login.style.opacity = "1";
        }else{
            formulario_register.style.display = "block";
            contenedor_login_register.style.left = "0px";
            formulario_login.style.display = "none";
            caja_trasera_register.style.display = "none";
            caja_trasera_login.style.display = "block";
            caja_trasera_login.style.opacity = "1";
        }
}



// var formulario = document.querySelector('#formulario');

// formulario.addEventListener('submit', function() {

//     var nombre = document.querySelector('#nombre').value;
//     var correo = parseEmail(document.querySelector('#correo').value);
//     var usuario = document.querySelector('#usuario').value;
//     var contrasena = parsePassword(document.querySelector('#contrasena').value);

//     if (nombre.trim()==null || nombre.trim().length==0){
//         document.querySelector('#error_nombre').innerHTML = "El nombre no es válido."
//         return false;
//     }else{
//         document.querySelector('#error_nombre').style.display= 'none';
//     }
//     if (correo.trim()==null || correo.trim().length==0){
//         document.querySelector('#error_correo').innerHTML = "El correo no es válido."
//         return false;
//     }else{
//         document.querySelector('#error_correo').style.display= 'none';
//     }
//     if (usuario.trim()==null || usuario.trim().length==0){
//         document.querySelector('#error_usuario').innerHTML = "El usuario no es válido."
//         return false;
//     }else{
//         document.querySelector('#error_usuario').style.display= 'none';
//     }
//     if (contrasena.trim()==null || contrasena.trim().length==0){
//         document.querySelector('#error_contrasena').innerHTML = "El contrasena no es válido."
//         return false;
//     }else{
//         document.querySelector('#error_contrasena').style.display= 'none';
//     }
    
// });
