<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="https://kit.fontawesome.com/bb4eb488b3.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="css/formulario.css">
    <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
    <title>Formulario1</title>
</head>
<body>
    <nav>
        <div class="logo"><h3>Pinolini</h3></i></div>
            <ul class="nav-links">
            <il>
                <a href="#">Home</a>
            </il>
            <il>
            <a href="#">About</a>
            </il>
            <il>
                <a href="#">Work</a>
            </il>
            <il>
                <a href="#">Project</a>
            </il>
            </ul>
            <div class="burger">
            <div class="line1"></div>
            <div class="line2"></div>
            <div class="line3"></div>
            </div>
    </nav>
    
    <main>
    <form action="php/registroDatos1.php" method="POST" class="formulario__infopersonal">
        <h2>Incripción - Ingenieria en sistema</h2> <br>
        <div class="textos"><i class="fas fa-check"></i>Nombre de usuario
            <input type="text" name="usuario">
        </div>
        <div class="textos"><i class="fas fa-check"></i>Nombre completo
            <input  type="text" name="nombre">
        </div>
        <div class="genre"><i class="fas fa-check"></i>Género
            <label for="hombre" class="radio-inline">Hombre
                <input class="generoselector" type="radio" name="genero" id="hombre" value="m"></label>
            <label for="mujer" class="radio-inline">Mujer
                <input class="generoselector" type="radio" name="genero" id="mujer" value="f"></label>
            <label for="otro" class="radio-inline">Otros
                <input class="generoselector" type="radio" name="genero" id="otro" value="o"></label>
        </div>
        <div class="textos"><i class="fas fa-check"></i>Año de nacimiento
            <input  type="number" name="nacimiento">
        </div>
        <button>Enviar</button>
    </form>
    </main>
    
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-col">
                    <h4>company</h4>
                    <ul>
                        <li><a href="#">about us</a></li>
                        <li><a href="#">our services</a></li>
                        <li><a href="#">privacy policy</a></li>
                        <li><a href="#">affiliate program</a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>get help</h4>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">shipping</a></li>
                        <li><a href="#">returns</a></li>
                        <li><a href="#">order status</a></li>
                        <li><a href="#">payment options</a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>online shop</h4>
                    <ul>
                        <li><a href="#">watch</a></li>
                        <li><a href="#">bag</a></li>
                        <li><a href="#">shoes</a></li>
                        <li><a href="#">dress</a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>follow us</h4>
                    <div class="social-links">
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/formulario.js"></script>
</body>
</html>