<?php
session_start();
if(!isset($_SESSION['usuario'])){
    echo'
    <script>
    alert("Please, enter into your account, or create one to enter into the page.");
    window.location = "index.php";
    </script>
    ';
    session_destroy();
    die();
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/oncein.css">
    <script src="https://kit.fontawesome.com/a076d05399.js" crossorigin="anonymous"></script>
    <link href="https://fonts.googleapis.com/css2?family=Baloo+Tammudu+2:wght@700&display=swap" rel="stylesheet">
    <title>OnceIn</title>
</head>
<body>
    <nav>
        <div class="logo"><h3>Uiversidad Eustral</h3></i></div>
            <ul class="nav-links">
            <il>
                <a href="#">Home</a>
            </il>
            <il>
            <a href="#">Formulario</a>
            </il>
            <il>
                <a href="#">Contactanos</a>
            </il>
            <il>
                <a href="#">SSS</a>
            </il>
            </ul>
            <div class="burger">
            <div class="line1"></div>
            <div class="line2"></div>
            <div class="line3"></div>
            </div>
    </nav>  

    <main>
        <h1 class="title">¿Cual es tu carrera de interés?</h1>
        <div class="container-box">
            <div class="box box1">
                <img src="images/ingsis.svg" alt="">
                <h2>Ingeniero en Sistema</h2>    
                <div class="container-p">
                    <p>An interdisciplinary field of engineering and engineering management that focuses on how to design, integrate, and manage complex systems over their life cycles.</p>
                </div>
                <div class="check">
                    <i class="fas fa-check"></i>
                </div>
            </div>
            <div class="box box2">
                <img src="images/dgraf.svg" alt="">
                <h2>Diseñador Gráfico</h2>
                <div class="container-p">
                    <p></p>
                </div>
                <div class="check">
                    <i class="fas fa-check"></i>
                </div>
            </div>
            <div class="box box3">
                <img src="images/dweb.svg" alt="">
                <h2>Desarrollador Web</h2>
                <div class="container-p">
                    <p></p>
                </div>
                <div class="check">
                    <i class="fas fa-check"></i>
                </div> 
            </div>
        </div>
        <button type="button" id="siguiente">Siguiente</button>
    </main>
    
    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-col">
                    <h4>company</h4>
                    <ul>
                        <li><a href="#">about us</a></li>
                        <li><a href="#">our services</a></li>
                        <li><a href="#">privacy policy</a></li>
                        <li><a href="#">affiliate program</a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>get help</h4>
                    <ul>
                        <li><a href="#">FAQ</a></li>
                        <li><a href="#">shipping</a></li>
                        <li><a href="#">returns</a></li>
                        <li><a href="#">order status</a></li>
                        <li><a href="#">payment options</a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>online shop</h4>
                    <ul>
                        <li><a href="#">watch</a></li>
                        <li><a href="#">bag</a></li>
                        <li><a href="#">shoes</a></li>
                        <li><a href="#">dress</a></li>
                    </ul>
                </div>
                <div class="footer-col">
                    <h4>follow us</h4>
                    <div class="social-links">
                        <a href="#"><i class="fab fa-facebook-f"></i></a>
                        <a href="#"><i class="fab fa-twitter"></i></a>
                        <a href="#"><i class="fab fa-instagram"></i></a>
                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/oncein.js"></script>

</body>
</html>
